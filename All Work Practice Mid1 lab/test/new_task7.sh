#! /bin/bash
names=$(ls)
for file in $names
do
	case "$(date +"%d-%m-%Y" -r $file)" in
	$(date +"%d-%m-%Y"))
		mv $file "new_$file"
		;;
	*)
		mv $file "old_$file"
		;;
esac		
done
